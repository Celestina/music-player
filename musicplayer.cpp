#include <iostream>
using namespace std;

struct node {
public:
		string	Title;
		string	Artist;
		node*	next;
};

struct List {
	
	public:
			List(void)	
			{
			head = NULL;	
			}
			
			//destructor to '~', nagdedeallocate ng memory; clean up class object
			~List(void);
			
			bool isEmpty()
			{ 
			return head == NULL;	
			}
			
			
			node* add(int index,string Title, string Artist);
			int Find(string x);
			int Delete(int index);
			void DisplayList(void);
			void Edit(int index, string newTitle, string Artist);
			string viewMsx(int index);
			
			
	private:
			node* head;
			node* artist;
};
int List::Find(string x){
	node* CurrNode	=	head;
	node* CurrArtist =	artist;
	int	CurrIndex	=	1;
	while (CurrNode && CurrNode->Title != x){
		CurrNode	=	CurrNode->next;
		CurrIndex++;
	}
	if (CurrNode) return CurrIndex;
	return 0;
}
int List:: Delete(int index){
	node* prevNode	=	NULL;
	node* CurrNode	=	head;
	node* prevArtist = NULL;
	node* CurrArtist = artist;
	int CurrIndex	=	1;
	while (CurrIndex != index){
		prevNode	=	CurrNode;
		CurrNode	=	CurrNode->next;
		prevArtist = CurrArtist;
		CurrArtist = CurrArtist->next;
		CurrIndex++;
	}
	if (CurrNode){
		if(prevNode && prevArtist){
			prevNode->next	=	CurrNode->next;
			prevArtist->next = CurrArtist->next;
			delete CurrNode;
		}
		else{
			head	=	CurrNode->next;
			artist = CurrArtist->next;
			delete	CurrNode;
			delete	CurrArtist;
		}
		return	CurrIndex;
	}
	return 0;
}
string List::viewMsx(int index){
	node* CurrNode	=	head;
	int	CurrIndex	=	1;
	while (CurrIndex != index){
		CurrNode	=	CurrNode->next;
		CurrIndex++;
	}
	if (CurrNode) return CurrNode->Title;
	//return NULL;
}

void List::DisplayList(){
	int num = 0;
	node* CurrNode	=	head;
	node* CurrArtist = 	artist;
	while (CurrNode != NULL &&CurrArtist != NULL){
		cout<<"["<<num+1<<"]"<<CurrNode->Title<<" ";
		cout<<" "<<"\tby "<<" "<<CurrArtist->Artist<<endl;;
		CurrArtist = CurrArtist->next;
		CurrNode	=	CurrNode->next;
		num++;
	}
}  

node* List::add(int index,string Title, string Artist){
	if (index < 0) return NULL;
	int CurrIndex = 1;
	node* CurrNode =head;
	node* CurrArtist = artist;
	while (CurrNode && index > CurrIndex){
		CurrNode	=	CurrNode->next;
		CurrIndex++;
	}
	while (CurrArtist && index > CurrIndex){
		CurrArtist	=	CurrArtist->next;
		CurrIndex++;
	}
	node* newNode =		new		node;
	newNode->Title = 	Title;
	node* newArtist = new node;
	newArtist->Artist = Artist;
	if (index==0){
			newNode->next	=	head;
			head 			=	newNode;
			newArtist->next = artist;
			artist = newArtist;
	}
	else{
		newNode->next	=	CurrNode->next;
		CurrNode->next	=	newNode;
		newArtist->next = CurrArtist->next;
		CurrArtist->next = newArtist;
	}
	return newNode;
}


List::~List(void){
	node* CurrNode	=	head,	*nextNode	=	NULL;
	while (CurrNode != NULL){
		nextNode	=	CurrNode->next;
		delete	CurrNode;
		CurrNode	=	nextNode;
	}
}

List Msx;



int main()

{
	node* CurrNode;
	string Title,Artist,editName,searchName;
	
	int select,del,edit,play,MsxSize;
	
	Msx.add(0,"Goodbyes","Post Malone");
	Msx.add(1,"Location","Khalid");
	Msx.add(2,"I.F.L.Y","Bazzi");
	
	
	cout<<"             WELCOME TO YOUR MSX\n\n\n[1] Add a Music\n\n\n[2] Delete a Music\n\n\n[3] Edit a Music Title\n\n\n[4] Play a Music\n\n\n[5] View Playlist\n\n\n ";
	cout<<"Please select desired transaction: ";
	cin>>select;
	switch(select)
	
	{
		
		
		case 1://adding song title
			cout<<"Enter Title of the song ";
			cin.ignore();
			getline(cin,Title);
			cout<<"Enter Artist > ";
			getline(cin,Artist);
			Msx.add(0,Title,Artist);
			cout<<"Current Playlist : \n";
			Msx.DisplayList();
		break;
		
		
		case 2://Delete a song 
			Msx.DisplayList();
			cout<<"Enter Number of Song title to be deleted  ";
			cin>>del;
			Msx.Delete(del);
			Msx.DisplayList();
		break;
		
		
		case 3://Edit a song
			Msx.DisplayList();
			cout<<"Select the number of the song title to be edited: ";
			cin>>edit;
			Msx.Delete(edit);
			cout<<"Enter the new song title : ";
			cin.ignore();
			getline(cin,editName);
			cout<<"Enter Name of New Artist > ";
			getline(cin,Artist);
			cout<<endl;
			Msx.add(edit-1,editName, Artist);
			Msx.DisplayList();
		break;
		
	
		case 4://Playing a song
			Msx.DisplayList();
			cout<<"Select the number of the song you want to play >  ";
			cin>>play;
			//nagssubtract sa index
			cout<<"Previous : "<<Msx.viewMsx(play-1)<<endl;
			cout<<"Now Playing : "<<Msx.viewMsx(play)<<endl;
			
			//nagaadd sa index
			cout<<"Next : "<<Msx.viewMsx(play+1)<<endl;	
		break;
		
		
		case 5://View playlist
			cout<<"\n\nYOUR PLAYLIST: "<<endl;
			Msx.DisplayList();
		break;
		default:
			cout<<"No such option.\n";
		break;
		if(cin.fail())
		{
			cout<<"Invalid Input.\n";
		}
	}
}